/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : blog_quasar

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 05/07/2022 22:18:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_settings
-- ----------------------------
DROP TABLE IF EXISTS `app_settings`;
CREATE TABLE `app_settings`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyField` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `valueField` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `app_settings_created_by_foreign`(`created_by`) USING BTREE,
  INDEX `app_settings_updated_by_foreign`(`updated_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of app_settings
-- ----------------------------
INSERT INTO `app_settings` VALUES ('21a6bee5-2df3-47ec-9553-42d62613effd', 'App.Favico', 'assets/img/app-settings/App_Favico_base_app-removebg-preview.png', 'image', 0, NULL, NULL, NULL, '2022-04-12 09:59:25');
INSERT INTO `app_settings` VALUES ('44dawc5d-113c-4cdc-871c-6c5bd7a24af8', 'App.Login.Cover', 'assets/img/app-settings/App_Login_Cover_base_app-removebg-preview.png', 'image', 0, NULL, NULL, NULL, '2022-04-12 09:59:25');
INSERT INTO `app_settings` VALUES ('5778eeda-5044-4f93-85ba-f242e0857898', 'App.Logo.Login', 'assets/img/app-settings/App_Logo_Login_base_app-removebg-preview.png', 'image', 0, NULL, NULL, NULL, '2022-04-12 09:59:25');
INSERT INTO `app_settings` VALUES ('5878d7f5-d473-4f43-b676-2ab542feecec', 'App.Logo', 'assets/img/app-settings/App_Logo_base_app-removebg-preview.png', 'image', 0, NULL, NULL, NULL, '2022-04-12 10:01:51');
INSERT INTO `app_settings` VALUES ('5fb7e9ea-73a4-4b20-abed-19e06c18966d', 'App.Name', 'Base App', 'text', 0, NULL, NULL, NULL, '2022-04-12 10:01:51');
INSERT INTO `app_settings` VALUES ('7aecf028-cf68-41d0-a486-6bd67e4082eb', 'App.Description', 'Description Base App', 'textarea', 0, NULL, NULL, NULL, '2022-04-12 10:01:51');

-- ----------------------------
-- Table structure for blogs
-- ----------------------------
DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of blogs
-- ----------------------------
INSERT INTO `blogs` VALUES ('485eaa3d-5b10-467a-ade4-3cb0f883a304', 'Introduction', 'Migrations are like version control for your database, allowing your team to define and share the application\'s database schema definition. If you have ever had to tell a teammate to manually add a column to their local database schema after pulling in your changes from source control, you\'ve faced the problem that database migrations solve.\r\n\r\nThe Laravel Schema facade provides database agnostic support for creating and manipulating tables across all of Laravel\'s supported database systems. Typically, migrations will use this facade to create and modify database tables and columns.', '1577698561_1657027914.jpg', 'blog_quasar.local/storage/blogs', '572003cd-0230-4534-b84d-eb2d9eab58a7', NULL, '2022-07-05 13:31:54', '2022-07-05 13:31:54');
INSERT INTO `blogs` VALUES ('9aa53432-9567-4d95-8541-25dc930e5cb3', 'Test Title', 'You may use the make:migration Artisan command to generate a database migration. The new migration will be placed in your database/migrations directory. Each migration filename contains a timestamp that allows Laravel to determine the order of the migrations:\r\n\r\nphp artisan make:migration create_flights_table\r\n\r\nLaravel will use the name of the migration to attempt to guess the name of the table and whether or not the migration will be creating a new table. If Laravel is able to determine the table name from the migration name, Laravel will pre-fill the generated migration file with the specified table. Otherwise, you may simply specify the table in the migration file manually.\r\n\r\nIf you would like to specify a custom path for the generated migration, you may use the --path option when executing the make:migration command. The given path should be relative to your application\'s base path.', '200197333_1657027872.png', 'blog_quasar.local/storage/blogs', '572003cd-0230-4534-b84d-eb2d9eab58a7', NULL, '2022-07-05 13:31:12', '2022-07-05 13:31:12');

-- ----------------------------
-- Table structure for comment_blogs
-- ----------------------------
DROP TABLE IF EXISTS `comment_blogs`;
CREATE TABLE `comment_blogs`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment_blogs
-- ----------------------------
INSERT INTO `comment_blogs` VALUES ('349f080c-db5a-41ec-8723-047973e381e1', '485eaa3d-5b10-467a-ade4-3cb0f883a304', 'Menarik sekali beritanya, di perbanyak lagi yaa', NULL, NULL, '2022-07-05 14:50:42', '2022-07-05 14:50:42');
INSERT INTO `comment_blogs` VALUES ('77519a6d-8b0a-4ae4-8249-91002eae4d97', '485eaa3d-5b10-467a-ade4-3cb0f883a304', 'Test komentar', NULL, NULL, '2022-07-05 15:04:22', '2022-07-05 15:04:22');
INSERT INTO `comment_blogs` VALUES ('de8ccf24-4bc7-40b3-992c-0dc75bc788c3', '485eaa3d-5b10-467a-ade4-3cb0f883a304', 'Mari kita budayakan kebersihan', NULL, NULL, '2022-07-05 14:56:04', '2022-07-05 14:56:04');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2021_11_06_162748_create_hotels_table', 1);
INSERT INTO `migrations` VALUES (2, '2021_11_07_070226_create_hotel_groups_table', 2);
INSERT INTO `migrations` VALUES (3, '2021_11_07_072353_create_pic_hotels_table', 3);
INSERT INTO `migrations` VALUES (4, '2021_11_07_072406_create_positions_table', 3);
INSERT INTO `migrations` VALUES (5, '2021_11_07_074842_create_facilities_table', 4);
INSERT INTO `migrations` VALUES (6, '2021_11_07_074901_create_facility_items_table', 4);
INSERT INTO `migrations` VALUES (7, '2021_11_07_080203_create_hotel_facilities_table', 5);
INSERT INTO `migrations` VALUES (8, '2021_11_07_082615_create_rooms_table', 6);
INSERT INTO `migrations` VALUES (9, '2019_12_14_000001_create_personal_access_tokens_table', 7);
INSERT INTO `migrations` VALUES (10, '2021_11_09_004108_create_memberships_table', 7);
INSERT INTO `migrations` VALUES (11, '2021_11_09_004407_create_religions_table', 7);
INSERT INTO `migrations` VALUES (12, '2021_11_09_004422_create_members_table', 7);
INSERT INTO `migrations` VALUES (13, '2021_11_10_103604_create_destionatios_table', 8);
INSERT INTO `migrations` VALUES (14, '2021_11_11_071155_create_destination_images_table', 9);
INSERT INTO `migrations` VALUES (15, '2021_11_12_065110_create_hotel_rooms_table', 10);
INSERT INTO `migrations` VALUES (16, '2021_11_14_151446_updated_hotels_list_table', 11);
INSERT INTO `migrations` VALUES (17, '2021_11_16_140938_updated_rooms_table', 12);
INSERT INTO `migrations` VALUES (18, '2021_11_16_144339_create_hotel_room_prices_table', 13);
INSERT INTO `migrations` VALUES (19, '2021_11_17_023446_update_hotels_table', 14);
INSERT INTO `migrations` VALUES (20, '2021_11_17_030826_create_hotel_deal_futures_table', 14);
INSERT INTO `migrations` VALUES (21, '2021_11_17_100801_update_kodepost_table', 15);
INSERT INTO `migrations` VALUES (22, '2021_11_17_101434_create_events_table', 15);
INSERT INTO `migrations` VALUES (23, '2021_11_18_062153_update_deal_features_table', 16);
INSERT INTO `migrations` VALUES (24, '2021_11_19_074957_create_password_resets_table', 17);
INSERT INTO `migrations` VALUES (25, '2021_11_20_110031_create_transactions_table', 18);
INSERT INTO `migrations` VALUES (26, '2021_11_20_113647_create_transaction_details_table', 18);
INSERT INTO `migrations` VALUES (27, '2021_11_20_113926_create_transaction_customers_table', 18);
INSERT INTO `migrations` VALUES (28, '2021_11_21_104324_create_member_memberships_table', 19);
INSERT INTO `migrations` VALUES (29, '2021_11_30_061012_create_payment_details_table', 20);
INSERT INTO `migrations` VALUES (30, '2022_07_05_123847_create_blogs_table', 21);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `permissions_created_by_foreign`(`created_by`) USING BTREE,
  INDEX `permissions_updated_by_foreign`(`updated_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('09c14694-89ee-4029-a7cf-76b0581be6de', 'App Settings Update', 'app-settings-update', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('19a085e0-7dc6-4b0e-8f6d-902dbd6f2382', 'Permissions Update', 'permissions-update', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('253aa8fa-94cb-4334-99cf-1c61942e4f62', 'Blogs Delete', 'blogs-delete', NULL, NULL, '2022-07-05 12:58:50', '2022-07-05 12:58:50');
INSERT INTO `permissions` VALUES ('2ab7755c-c49c-4b03-897b-9f603bd3e628', 'Permissions Create', 'permissions-create', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('41d9d910-060f-45c6-96e0-85a3cb395583', 'Roles View', 'role-view', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('4bbcb4ab-c7d2-460a-bad9-051dce67b8ec', 'Role Config', 'role-config', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('5745df0a-d004-40c0-9ed4-22ed6d182ec2', 'Roles Create', 'role-create', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('5f5c77e4-068b-4130-918e-e5a0abc2d26a', 'Roles Update', 'role-update', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('685a7f4e-2375-41c9-8555-d3dec90e25ea', 'Roles Edit', 'role-edit', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('6861eddc-1851-42bb-8af0-224fbb8b1eab', 'App Settings View', 'app-settings-view', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('87b8a2c3-bcf0-457c-98bd-2f3b6a315e03', 'Url Create', 'url-create', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('95192f3c-78af-4819-b116-e6f05fe43e92', 'Blogs Create', 'blogs-create', NULL, NULL, '2022-07-05 12:58:50', '2022-07-05 12:58:50');
INSERT INTO `permissions` VALUES ('9c4f366a-dd8a-4850-bc63-aad3594e42d9', 'Url Edit', 'url-edit', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('a4a96503-d531-4ba6-b676-7f674a7bf59e', 'Blogs View', 'blogs-view', NULL, NULL, '2022-07-05 12:58:50', '2022-07-05 12:58:50');
INSERT INTO `permissions` VALUES ('a6e81fe4-e1b9-40a3-8bb5-8c9e4703d6c9', 'Url Update', 'url-update', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('b384dab1-4f82-4cae-a96a-4ccb6dcf4979', 'Url View', 'url-view', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('c1c0e8a2-ccfa-4ada-a9a6-a65d38a6bd13', 'Blogs Update', 'blogs-update', NULL, NULL, '2022-07-05 12:58:50', '2022-07-05 12:58:50');
INSERT INTO `permissions` VALUES ('c3ed0e19-adce-4a51-af73-d8b50dd4f115', 'Permissions Generate', 'permissions-generate', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('c97f7b86-2101-4d14-8090-3ff395271564', 'Permissions Edit', 'permissions-edit', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('ceb86827-c3d9-4cf0-b637-3b18abd2a239', 'Permissions View', 'permissions-view', '1', NULL, '2021-11-08 03:09:10', NULL);
INSERT INTO `permissions` VALUES ('edcae67d-1424-4672-b5bb-a98cf1a9184e', 'Blogs Edit', 'blogs-edit', NULL, NULL, '2022-07-05 12:58:50', '2022-07-05 12:58:50');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_created_by_foreign`(`created_by`) USING BTREE,
  INDEX `role_updated_by_foreign`(`updated_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('ab1658ed-af25-4864-b13d-d821e74d2a9c', 'Administrator', '572003cd-0230-4534-b84d-eb2d9eab58a7', NULL, '2022-04-15 08:06:43', '2022-04-15 08:06:43');

-- ----------------------------
-- Table structure for role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_permissions_created_by_foreign`(`created_by`) USING BTREE,
  INDEX `role_permissions_updated_by_foreign`(`updated_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES ('1b6af991-f601-4fa8-a4c8-a85e563ecafa', 'ceb86827-c3d9-4cf0-b637-3b18abd2a239', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('3008dc4f-59e3-4a57-9498-24f171fb7cda', '5745df0a-d004-40c0-9ed4-22ed6d182ec2', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('36e0282b-2c16-433b-ad55-23509de01350', 'a4a96503-d531-4ba6-b676-7f674a7bf59e', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('3a450f7e-195b-42d7-a67f-64de7c8d76e5', '95192f3c-78af-4819-b116-e6f05fe43e92', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('3ea0fe84-df9b-4c1f-bba1-a3c0dc226407', 'c97f7b86-2101-4d14-8090-3ff395271564', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('440c0fd2-c223-4245-8900-978f9f3e216d', '9c4f366a-dd8a-4850-bc63-aad3594e42d9', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('4ae8b70f-17b1-42e2-a1ae-5e7c5dea26cb', '41d9d910-060f-45c6-96e0-85a3cb395583', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('522fcd38-9b16-4e19-9dbb-9be77299ec09', '2ab7755c-c49c-4b03-897b-9f603bd3e628', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('552e5d17-b932-45ca-9269-99e4238ad630', '87b8a2c3-bcf0-457c-98bd-2f3b6a315e03', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('593877c0-4abd-49d1-90a1-6548849225d8', 'c1c0e8a2-ccfa-4ada-a9a6-a65d38a6bd13', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('67f36d9c-e47d-41e7-bed3-350d887450a2', '4bbcb4ab-c7d2-460a-bad9-051dce67b8ec', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('7c376c89-dbbc-4b17-80d6-33157c1c732a', 'b384dab1-4f82-4cae-a96a-4ccb6dcf4979', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('8c39a6e8-1465-45e4-b9d8-fa7d5457e790', '5f5c77e4-068b-4130-918e-e5a0abc2d26a', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('a2399bad-63dd-48c6-8458-1b04094f7f08', '09c14694-89ee-4029-a7cf-76b0581be6de', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('a27e5837-09f1-4009-88b8-0703a33ef265', '19a085e0-7dc6-4b0e-8f6d-902dbd6f2382', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('b1003c96-da3a-4d8e-91e0-7ff7a6fcef27', 'a6e81fe4-e1b9-40a3-8bb5-8c9e4703d6c9', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('bb7a2374-220d-4e22-80cd-e47907c3d1aa', '6861eddc-1851-42bb-8af0-224fbb8b1eab', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('bcaefdef-58a5-44ca-acc1-5d2fbc91c619', '253aa8fa-94cb-4334-99cf-1c61942e4f62', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('c5d64c0a-55d0-4b12-87d0-630e398508f2', 'edcae67d-1424-4672-b5bb-a98cf1a9184e', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('ecfa4780-d3b0-458a-b712-f7fe471bde90', 'c3ed0e19-adce-4a51-af73-d8b50dd4f115', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');
INSERT INTO `role_permissions` VALUES ('f25d6bf9-c50e-4ab5-aa9d-c2c22c4aa1c0', '685a7f4e-2375-41c9-8555-d3dec90e25ea', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:06:09', '2022-07-05 13:06:09');

-- ----------------------------
-- Table structure for url
-- ----------------------------
DROP TABLE IF EXISTS `url`;
CREATE TABLE `url`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `order` int(11) NOT NULL,
  `position` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `url_created_by_foreign`(`created_by`) USING BTREE,
  INDEX `url_updated_by_foreign`(`updated_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of url
-- ----------------------------
INSERT INTO `url` VALUES ('0ed16ef3-1045-4385-a277-2da1b87ba763', NULL, 'App Setting', 'app-settings', 'settings', 1, 'settings', '572003cd-0230-4534-b84d-eb2d9eab58a7', NULL, '2022-04-15 08:12:50', '2022-04-15 08:12:50');
INSERT INTO `url` VALUES ('36211f55-49de-4f3a-975e-61068a7ad551', NULL, 'Permissions', 'permissions', 'lock', 3, 'settings', '572003cd-0230-4534-b84d-eb2d9eab58a7', NULL, '2022-04-15 08:14:40', '2022-04-15 08:14:40');
INSERT INTO `url` VALUES ('a0c529a6-532f-4c84-98a5-9b789f658038', NULL, 'Url', 'url', 'link', 2, 'settings', '572003cd-0230-4534-b84d-eb2d9eab58a7', NULL, '2022-04-15 08:14:03', '2022-04-15 08:14:03');
INSERT INTO `url` VALUES ('a5827aa5-4e2d-488d-95c6-e31452f5da3c', NULL, 'Roles', 'role', 'users', 4, 'settings', '572003cd-0230-4534-b84d-eb2d9eab58a7', NULL, '2022-04-15 08:15:02', '2022-04-15 08:15:02');
INSERT INTO `url` VALUES ('b3a77c83-e183-4262-906d-8af5334742ee', NULL, 'Blogs', 'blogs', 'books', 1, 'menu utama', NULL, NULL, '2022-07-05 12:56:05', '2022-07-05 12:56:05');

-- ----------------------------
-- Table structure for url_access
-- ----------------------------
DROP TABLE IF EXISTS `url_access`;
CREATE TABLE `url_access`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `url_access_created_by_foreign`(`created_by`) USING BTREE,
  INDEX `url_access_updated_by_foreign`(`updated_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of url_access
-- ----------------------------
INSERT INTO `url_access` VALUES ('2c18bdfe-962e-466e-b0e1-1f4d5af849be', '36211f55-49de-4f3a-975e-61068a7ad551', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:00:10', '2022-07-05 13:00:10');
INSERT INTO `url_access` VALUES ('7fc3b71d-477a-4032-a343-b3ae5b21aab1', 'b3a77c83-e183-4262-906d-8af5334742ee', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:00:10', '2022-07-05 13:00:10');
INSERT INTO `url_access` VALUES ('84240314-8a64-4f38-b088-c152b41df690', 'a5827aa5-4e2d-488d-95c6-e31452f5da3c', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:00:11', '2022-07-05 13:00:11');
INSERT INTO `url_access` VALUES ('96c87400-92c4-455f-81f4-fca6214a8165', '0ed16ef3-1045-4385-a277-2da1b87ba763', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:00:10', '2022-07-05 13:00:10');
INSERT INTO `url_access` VALUES ('ea078165-a25f-4ad2-a2d2-13fa26433c33', 'b1a4a6af-11f6-408e-8af2-656bd7eba645', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:00:11', '2022-07-05 13:00:11');
INSERT INTO `url_access` VALUES ('fdffdb84-58b1-40c6-ba64-adc70b4cc67e', 'a0c529a6-532f-4c84-98a5-9b789f658038', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', NULL, NULL, '2022-07-05 13:00:10', '2022-07-05 13:00:10');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `role_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  INDEX `users_role_id_foreign`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('05dc1eb4-783f-4492-90da-04f43f73a19d', 'Administrator', 'admin@gmail.com', NULL, NULL, '$2y$10$/6l51sy5gCh6ckb9H3cfm.dFxyGT3PraWOP0KSEEpNGmQhy8YZboa', NULL, NULL, NULL, '2022-04-15 06:20:05', '2022-04-15 06:20:05');
INSERT INTO `users` VALUES ('572003cd-0230-4534-b84d-eb2d9eab58a7', 'Tengku Firmansyah', 'tengkufirmansyah2@gmail.com', '2022-04-15 06:47:17', 'ab1658ed-af25-4864-b13d-d821e74d2a9c', '$2y$10$7bO9qFCNx8fXnwgM1whduuB9eyYQDFRQJXxwB8RtsYq2NE40ZpMTK', '5Lh5DBadFsdDf1EaIJvVlS17lzKFE8WZY1GWzgY7cvMtCJV0bR0KgJbPNZcp', NULL, NULL, '2022-04-15 06:41:34', '2022-04-15 07:40:51');

SET FOREIGN_KEY_CHECKS = 1;

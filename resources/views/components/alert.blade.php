@if(Session::has('alert'))
<div class="alert alert-custom alert-outline-2x alert-outline-{{ Session::get('alert-class') ? Session::get('alert-class') : 'primary' }} fade show mb-5" role="alert">
    <div class="alert-icon"><i class="flaticon-{{ Session::get('alert-icon') ? Session::get('alert-icon') : 'warning' }}"></i></div>
    <div class="alert-text">{{ Session::get('message') }}</div>
    <div class="alert-close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="ki ki-close"></i></span>
        </button>
    </div>
</div>
@elseif(Session::has('message'))
<div class="alert alert-{{Session::get('alert-class')}} alert-dismissible fade show" role="alert">
    <div class="alert-icon">
        <i class="flaticon-questions-circular-button"></i>
    </div>
    <div class="alert-text">
        {{Session::get('message')}}
    </div>
    <div class="alert-close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="ki ki-close"></i></span>
        </button>
    </div>
</div>
@endif
@if ($errors->any())
<div class="alert alert-custom alert-danger" role="alert">
    <div class="alert-icon">
        <i class="flaticon-questions-circular-button"></i>
    </div>
    <div class="alert-text">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>
</div>
@endif
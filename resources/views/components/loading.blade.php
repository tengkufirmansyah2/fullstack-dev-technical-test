<div class="modal fade" id="loadingpage" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" data-backdrop="static" aria-hidden="true" style="background-color: rgb(0 0 0 / 30%);">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="spinner spinner-success spinner-center spinner-lg"></div>
    </div>
</div>
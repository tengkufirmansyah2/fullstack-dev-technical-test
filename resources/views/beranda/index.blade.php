@extends('components.template')
@section('title','Beranda')
@section('content')

<div class="container-fluid p-0">

    <h1 class="h3 mb-3">Dashboard</h1>

    <div class="row">
        <div class="col-12 col-lg-4 col-xxl-3 d-flex">
            <h6>Welcome <strong>{{Auth::user()->name}}</strong> </h6>
        </div>
    </div>

</div>
@endsection
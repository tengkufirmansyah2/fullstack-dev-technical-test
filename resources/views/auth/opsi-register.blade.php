@extends('auth.template')
@section('title','Pilih Opsi Pendaftaran')
@section('content')
<div class="text-center pb-8">
    <h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Pilih Opsi Pendaftaran</h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Body-->
            <div class="card-body d-flex align-items-center py-0 mt-8">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-5">
                    <a href="{{ route('daftar.pegawai') }}" class="card-title font-weight-bolder text-dark-75 font-size-h5 mb-2 text-hover-primary">Pegawai</a>
                    <span class="font-weight-bold text-muted font-size-lg">Daftar sebagai pegawai</span>
                </div>
                <img src="{{ URL::asset('assets/media/svg/avatars/029-boy-11.svg') }}" alt="" class="align-self-end h-100px">
            </div>
            <!--end::Body-->
        </div>
    </div>
</div>
@endsection
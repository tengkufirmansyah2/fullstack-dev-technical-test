@extends('auth.template')
@section('title','Daftar Akun')
@section('content')
<div class="login-form login-signin py-11">
	<form class="form" action="{{ route('daftar.buat-akun.sekarang',[app('request')->id,app('request')->token]) }}" id="kt_login_signin_form" method="POST">
		@csrf
		<div class="text-center pb-10">
			<h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Form Daftar Akun</h2>
		</div>
		@include('components.alert')
		<div class="form-group">
			<label class="font-size-h6 font-weight-bolder text-dark">Nama Anda</label>
			<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="text" disabled autocomplete="off" required value="{{ $nama }}" />
		</div>
		<div class="form-group">
			<label class="font-size-h6 font-weight-bolder text-dark">Username</label>
			<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="text" name="username" autocomplete="off" required placeholder="Isi Username" value="{{ old('username') }}" />
		</div>
		<div class="form-group">
			<div class="d-flex justify-content-between mt-n5">
				<label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>
			</div>
			<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="password" name="password" required autocomplete="off" value="" placeholder="Password" />
		</div>
		<div class="form-group">
			<div class="d-flex justify-content-between mt-n5">
				<label class="font-size-h6 font-weight-bolder text-dark pt-5">Ketik Ulang Password</label>
			</div>
			<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="password" name="password_confirm" required autocomplete="off" value="" placeholder="Ketik Ulang Password" />
		</div>
		<div class="text-center pt-2">
			<button type="submit" class="btn btn-dark btn-block font-weight-bolder font-size-h6 px-8 py-4 my-3">Buat Akun</button>
		</div>
	</form>
</div>
@endsection
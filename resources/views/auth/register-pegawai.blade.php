@extends('auth.template')
@section('title','Pilih Opsi Pendaftaran')
@section('content')
<div class="login-form login-signin py-11">
	<form class="form" action="{{ route('daftar.pegawai.check') }}" id="kt_login_signin_form" method="POST">
		@csrf
		<div class="text-center pb-10">
			<h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Form Daftar Akun</h2>
			<span class="text-muted font-weight-bold font-size-h4">Sudah ada akun?
				<a href="{{ route('login') }}" id="kt_login_signup" class="text-primary font-weight-bolder">Login ke Aplikasi</a></span>
		</div>
		@include('components.alert')
		<div class="form-group">
			<label class="font-size-h6 font-weight-bolder text-dark">Nomor Induk Pegawai</label>
			<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="text" name="nip" autocomplete="off" required placeholder="Nomor Induk Pegawai (NIP)" value="{{ old('nip') }}" />
		</div>
		<div class="form-group">
			<div class="d-flex justify-content-between mt-n5">
				<label class="font-size-h6 font-weight-bolder text-dark pt-5">Tanggal Lahir</label>
			</div>
            <div class="input-group date">
                <input type="text" class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" name="tanggal_lahir" id="tanggallahir" autocomplete="off" readonly="readonly" value="{{ old('tanggal_lahir') }}" placeholder="Tanggal Lahir" value="" />
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar-check-o"></i>
                    </span>
                </div>
            </div>   
		</div>
		<div class="form-group">
			<div class="d-flex justify-content-between mt-n5">
				<label class="font-size-h6 font-weight-bolder text-dark pt-5">Email</label>
			</div>
			<input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg" type="email" name="email" autocomplete="off" value="{{ old('email') }}" placeholder="Email" />
		</div>
		<div class="text-center pt-2">
			<button type="submit" class="btn btn-dark btn-block font-weight-bolder font-size-h6 px-8 py-4 my-3">Daftar Sekarang!</button>
		</div>
	</form>
</div>
@endsection
@section('js')
<script src="{{ URL::asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
<script>
    $('#tanggallahir').datepicker({
            rtl: false,
            todayHighlight: true,
            orientation: "bottom left",
            format: "yyyy-mm-dd",
            defaultViewDate: { year: 1990, month: 0, day: 01 },
            templates: {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            }
        });
</script>
@endsection
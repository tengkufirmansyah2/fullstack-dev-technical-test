@extends('auth.template')
@section('title','Login')
@section('content')
<div class="text-center mt-4">
	<img src="{{ URL::asset('/').app_setting()['App_Logo_Login']['valueField'] }}" style="max-width: 500px; width: 100%" alt="" />
</div>

<div class="card">
	<div class="card-body">
		<div class="m-sm-4">
			<div class="text-center">
				<h2>Login</h2>
			</div>
			<form class="form" action="{{ route('auth.login') }}" method="POST">
				@csrf
				@include('components.alert')
				<div class="mb-3">
					<label class="form-label">Email</label>
					<input class="form-control form-control-lg" type="email" name="email" placeholder="Enter your email" />
				</div>
				<div class="mb-3">
					<label class="form-label">Password</label>
					<input class="form-control form-control-lg" type="password" name="password" placeholder="Enter your password" />
					<small>
						<a href="index.html">Forgot password?</a>
					</small>
				</div>
				<div>
					<label class="form-check">
						<input class="form-check-input" type="checkbox" value="remember-me" name="remember-me" checked>
						<span class="form-check-label">
							Remember me next time
						</span>
					</label>
				</div>
				<div class="text-center mt-3">
					<button type="submit" class="btn btn-lg btn-primary">Login</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@extends('components.template')
@section('title',$title)
@section('content')
<div class="container-fluid p-0">
    <div class="mb-3">
        <h1 class="h3 d-inline align-middle"><strong>{{$title}}</strong></h1>
        <a class="badge bg-dark text-white ms-2" href="{{ route('app.blogs.index') }}">
            < Kembali
        </a>
    </div>
    <form id="form">
        <input type="hidden" name="_method" value="{{ $edit == true ? 'PATCH' : 'POST' }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">Hotel Detail</h3>
                    </div>
                    <div class="card-body">
                        <div class="validation-message"></div>
                        @include ('components.caution', ['title' => $title])
                        @include ('blogs.form', ['formMode' => 'create'])
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" id="submit" class="btn btn-primary mr-2">{{ $edit == true ? 'Ubah Data' : 'Tambah Baru' }}</button>
            <a href="{{ route('app.blogs.index') }}" class="btn btn-danger mr-2">Kembali</a>
        </div>
    </form>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        const fv = FormValidation.formValidation(
            document.getElementById('form'),
            {
                fields: {
                    title: {
                        validators: {
                            notEmpty: {
                                message: 'Title!'
                            },
                            stringLength: {
                                max:225,
                                message: 'Maximum is 225 characters.'
                            }
                        }
                    },
                    description: {
                        validators: {
                            notEmpty: {
                                message: 'description!'
                            }
                        }
                    },
                    @if($edit == false)
                    image: {
                        validators: {
                            notEmpty: {
                                message: 'Image!'
                            }
                        }
                    }
                    @endif
                },
                plugins:{
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    SubmitButton: new FormValidation.plugins.SubmitButton()
                }
            }
        ).on('core.form.valid', function() {
            var formData = $("#form").serialize();
            showLoading();
            $.ajax({
                url: "{{ $edit == true ? route('app.blogs.update',[app('request')->blog]) : route('app.blogs.store') }}",
                type: "POST",
                dataType: "JSON",
                data: new FormData($('#form')[0]),
                processData: false,
                contentType: false,
                success: function (data)
                {
                    Swal.fire("Berhasil!", "{{ $edit == true ? 'Successfully changed data blogs!' : 'New blogs data added successfully!' }}", "success");
                    hideLoading();
                    window.location.href = "{{ route('app.blogs.index') }}";
                },
                error: function (xhr, status, error)
                {
                    var errs = '<ul style="display:block">'
                    if(xhr.status == 422){
                        p = xhr.responseJSON.errors;
                        for(var key in p){
                            p[key].forEach(err => {
                                errs += '<li><label class="error">'+err+'</label></li>'
                            })
                        }
                        errs += '</ul>';
                    }else{
                        errs += '<li><label class="error">'+xhr.responseJSON.message+'</label></li>';
                    }
                    $('.validation-message').html(errs)
                    hideLoading()
                }
            });
        });
    });

    var image1 = new KTImageInput('kt_image_1');

</script>
@endsection


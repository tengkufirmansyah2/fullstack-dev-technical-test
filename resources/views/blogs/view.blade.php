@extends('components.template')
@section('title',$title)
@section('content')
<div class="container-fluid p-0">
    @include('components.alert')
    <div class="mb-3">
        <h1 class="h3 d-inline align-middle"><strong>{{$title}}</strong></h1>
        <a class="badge bg-dark text-white ms-2" href="{{ route('app.blogs.index') }}">
            < Kembali
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">
                        City&nbsp;
                    </h3>
                </div>
                <div class="card-body row">
                    <div class="col-md-7">
                        <table border="0" class="table-vertical-top">
                            <tr>
                                <th>Title</th>
                                <th> : </th>
                                <td>{{$data->title}}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <th> : </th>
                                <td>{{$data->description}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <div class="mb-3">
                            <label class="form-label">Main Image : </label></br>
                            <img src="http://{{$data->image_path.'/thumbnail/'.$data->image_name }}" style="max-width: 200px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
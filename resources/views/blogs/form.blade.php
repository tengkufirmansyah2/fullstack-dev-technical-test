<div class="form-group">
    <label>Title<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="title" id="title" value="{{ isset($data->title) ? $data->title : ''}}" required>
</div>
<div class="form-group">
    <label>Description<span class="text-danger">*</span></label>
    <textarea name="description" class="form-control">{{isset($data->description) ? $data->description : ''}}</textarea>
</div>

<div class="form-group row">
    <label>Image <span class="text-danger">*</span></label>
    <div class="col-lg-9 col-xl-6">
        <div class="image-input image-input-outline" id="kt_image_1">
            <div class="image-input-wrapper" style="background-image: url(//{{ isset($data) ? $data->image_path.'/'.$data->image_name : '' }})"></div>
            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                <i class="fa fa-pen icon-sm text-muted"></i>
                <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                <input type="hidden" name="image_remove"/>
            </label>

            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                <i class="ki ki-bold-close icon-xs text-muted"></i>
            </span>
        </div>
        <span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
    </div>
</div>
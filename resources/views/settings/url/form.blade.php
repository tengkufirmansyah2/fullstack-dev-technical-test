<div class="form-group">
    <label>Parent</label>
    <select class="form-control" name="parent_id" id="parent_id">
        <option> </option>
        @foreach($parent as $val)
        <option value="{{$val->id}}" {{ isset($url->parent_id) ? $url->parent_id == $val->id ? 'selected' : '' : '' }}>{{$val->name}}</option>
        @endforeach
    </select>
    <span class="form-text text-muted">Pilih apabila memiliki induk</span>
</div>
<div class="form-group">
    <label>Nama<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="name" id="name" value="{{ isset($url->name) ? $url->name : ''}}" >
</div>
<div class="form-group">
    <label>Url<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="url" id="url" value="{{ isset($url->url) ? $url->url : ''}}" >
</div>
<div class="form-group">
    <label>Icon</label>
    <textarea class="form-control" name="icon" rows="3" placeholder="icon url">{{ $edit == true ? $url->icon : old('icon') }}</textarea>
</div>
<div class="form-group">
    <label>Order<span class="text-danger">*</span></label>
    <input type="number" class="form-control" name="order" id="order" value="{{ isset($url->order) ? $url->order : ''}}" >
</div>
<div class="form-group">
    <label>Position<span class="text-danger">*</span></label>
    <select class="form-control" name="position" id="position">
        <?php if (isset($url->position)) { ?>
        <option value="{{$url->position}}">{{$url->position}}</option>
        <?php } ?>
        <option> </option>
        <option value="settings">Settings</option>
        <option value="master">Master</option>
        <option value="menu utama">Menu Utama</option>
    </select>
</div>
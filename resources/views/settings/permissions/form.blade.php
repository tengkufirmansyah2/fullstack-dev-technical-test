<div class="form-group">
    <label>Nama<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="name" id="name" value="{{ isset($permissions->name) ? $permissions->name : ''}}" required="true">
</div>
<div class="form-group">
    <label>Slug<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="slug" id="slug" value="{{ isset($permissions->slug) ? $permissions->slug : ''}}" required="true">
</div>
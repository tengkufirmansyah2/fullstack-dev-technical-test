<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthCtrl;
use App\Http\Controllers\AppSettingsCtrl;
use App\Http\Controllers\UrlCtrl;
use App\Http\Controllers\RoleCtrl;
use App\Http\Controllers\PermissionsCtrl;
use App\Http\Controllers\RegisterCtrl;
use App\Http\Controllers\BerandaCtrl;
use App\Http\Controllers\BlogsCtrl;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return redirect('login');
});
Route::get('login', [AuthCtrl::class, 'index'])->name('login');
Route::post('signin',[AuthCtrl::class,'login'])->name('auth.login');
Route::get('/daftar',[RegisterCtrl::class,'daftar'])->name('daftar');
Route::get('/daftar/pegawai',[RegisterCtrl::class,'daftarPegawai'])->name('daftar.pegawai');
Route::post('/daftar/pegawai',[RegisterCtrl::class,'checkDaftarPegawai'])->name('daftar.pegawai.check');

Route::get('/register/create-account/verify/{id}/{token}',[RegisterCtrl::class,'verifyRegister'])->name('daftar.buat-akun');
Route::post('/register/create-account/verify/{id}/{token}',[RegisterCtrl::class,'verifyRegisterNow'])->name('daftar.buat-akun.sekarang');

Route::get('/getcities/{id}', [BerandaCtrl::class,'getCities'])->name('getcities');

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::group(['prefix' => 'app','as' => 'app.'],function(){
            
        Route::get('/',[BerandaCtrl::class,'index'])->name('beranda');
        Route::get('/beranda',[BerandaCtrl::class,'index'])->name('beranda.index');

        Route::match(['GET','POST'],'app-settings', [AppSettingsCtrl::class,'index'])->name('app_setting');

        Route::resource('/url',UrlCtrl::class);
        Route::get('url/delete/{id}', [UrlCtrl::class,'destroy']);

        Route::resource('/permissions',PermissionsCtrl::class);
        Route::get('permissions/delete/{id}', [PermissionsCtrl::class,'destroy']);
        Route::get('permissions/generate/{id}', [PermissionsCtrl::class,'generate']);

        Route::resource('/role',RoleCtrl::class);
        Route::get('role/delete/{id}', [RoleCtrl::class,'destroy']);
        Route::match(array('GET','POST'),'/role/configuration-permissions/{id}', [RoleCtrl::class,'permission']);
        Route::match(array('GET','POST'),'/role/configuration-menus/{id}', [RoleCtrl::class,'menu']);

        Route::resource('/blogs',BlogsCtrl::class);
        Route::get('blogs/delete/{id}', [BlogsCtrl::class,'destroy']);

        Route::get('/signout',[AuthCtrl::class,'signout'])->name('auth.logout');
    });
});
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogsCtrl;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('blogs', [BlogsCtrl::class,'getBlogs']);
Route::get('blogs/{id}', [BlogsCtrl::class,'getDetailBlogs']);
Route::post('comment-blogs/{id}', [BlogsCtrl::class,'commentBlogs']);

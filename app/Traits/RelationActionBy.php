<?php

namespace App\Traits;

use App\Models\Users;
use Auth;

trait RelationActionBy {
    protected static function boot()
    {
        parent::boot();
        
        static::creating(function ($model) {
            try {
                if(Auth::check()){
                    if($model->created_by){
                        $model->created_by = Auth::User()->id;
                    }
                }
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });

        static::updating(function ($model) {
            try{
                if(Auth::check()){
                    $model->updated_by = Auth::user()->id;
                }
            }catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });

        static::deleting(function($model) {
            try{
                if(Auth::check()){
                    $model->deleted_by = Auth::user()->id;
                }
            }catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}

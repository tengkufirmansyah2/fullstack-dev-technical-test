<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PegawaiAktifScope implements Scope
{
    /**
     * Restrict results to users aged 18 or over.
     *
     * @param Builder $builder
     * @param Model  $model
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereIn('status_kerja', ['kerja','cuti','keluar']);
    }
}
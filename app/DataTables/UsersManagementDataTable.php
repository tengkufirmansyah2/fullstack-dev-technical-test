<?php

namespace App\DataTables;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Auth;

class UsersManagementDataTable extends DataTable
{
    protected $request = [];

    public function __construct(Request $request){
        $this->request = $request;
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('pegawai.nama',function($model){
                if($model->pegawai == null){
                    return $model->name;
                }
                return $model->pegawai->nama;
            })
            ->editColumn('pegawai.lembaga.nama',function($model){
                if($model->pegawai == null){
                    return '-';
                }
                return $model->pegawai->lembaga->nama;
            })
            ->editColumn('pegawai.jabatan.nama',function($model){
                if($model->pegawai == null){
                    return '-';
                }
                return $model->pegawai->jabatan->nama;
            })
            ->editColumn('role.name',function($model){
                return ucfirst($model->role->name);
            })
            ->addColumn('action','users-management.action.button')
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SekolahTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = User::select('users.*')
                    ->with('role','pegawai.lembaga','pegawai.jabatan');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->ajax('')
                ->parameters([
                    'dom' => 'lfrtip',
                    'lengthMenu' => [ 10, 50, 100, 200, 300, 500 ],
                    'order' => [
                        [1,'asc']
                    ]
                ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
            'action' => [
                'width' => "5%",
                'orderable' => false
            ],
            'pegawai.nama' => [
                'title' => 'Nama'
            ],
            'pegawai.lembaga.nama' => [
                'title' => 'Lembaga'
            ],
            'pegawai.jabatan.nama' => [
                'title' => 'Jabatan'
            ],
            'username',
            'email',
            'role.name' => [
                'title' => 'Role'
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'UsersManagement_' . date('YmdHis');
    }
}

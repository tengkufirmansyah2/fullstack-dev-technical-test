<?php

namespace App\DataTables;

use App\Models\Jabatan;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Auth;

class JabatanDataTable extends DataTable
{
    protected $request = [];

    public function __construct(Request $request){
        $this->request = $request;
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('lembaga.nama',function($model){
                if($model->lembaga == null){
                    return "-";
                }
                return $model->lembaga->nama;
            })
            ->editColumn('tunjangan',function($model){
                return 'Rp. '.number_format($model->tunjangan);
            })
            ->addColumn('action','jabatan.action.button')
            ->rawColumns(['action','lembaga.nama','tunjangan']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SekolahTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Jabatan::query()
                        ->select('jabatan.*')
                        ->with('lembaga');
        if(Auth::User()->sekolah_id){
            $query = Jabatan::query()
                            ->select('jabatan.*')
                            ->with('lembaga')
                            ->where('jabatan.sekolah_id',Auth::User()->sekolah_id);
        }
        if(request()->has('lembaga_id')){
            $query->where('lembaga_id',request()->lembaga_id);
        }
        if(request()->has('nama')){
            $query->where('jabatan.nama','LIKE','%'.request()->nama.'%');
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->ajax('')
                ->parameters([
                    'dom' => 'lfrtip',
                    'lengthMenu' => [ 10, 50, 100, 200, 300, 500 ],
                ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
            'action' => [
                'width' => "5%"
            ],
            'lembaga.nama' => [
                'title' => 'Lembaga',
                'orderable' => true
            ],
            'jenis_pegawai' => [
                'orderable' => true
            ],
            'nama' => [
                'orderable' => true
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'JabatanPegawaiTable_' . date('YmdHis');
    }
}

<?php

namespace App\DataTables;

use App\Models\Pegawai;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Auth;
class PegawaiDataTable extends DataTable
{
    protected $request = [];
    protected $exportColumns = [
        ['data' => 'nip', 'title' => 'Nama'],
        ['data' => 'nip', 'title' => 'Nip'],
    ];
    protected $printColumns  = [
        ['data' => 'nip', 'title' => 'Nama'],
        ['data' => 'nip', 'title' => 'Nip'],
    ];
    public function __construct(Request $request){
        $this->request = $request;
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query, Request $request)
    {
        if(is_null(Auth::User()->sekolah_id)){
            return datatables()
                ->eloquent($this->query())
                ->addColumn('action','pegawai.action.button')
                ->editColumn('jabatan.nama',function($model){
                    return $model->jenis_pegawai.'/'.$model->jabatan->nama;
                })
                ->editColumn('tanggal_lahir',function($model){
                    return $model->usia();
                })
                ->rawColumns(['action','jabatan.nama','foto']);
        }
        return datatables()
            ->eloquent($this->query())
            ->addColumn('action','pegawai.action.button')
            ->editColumn('jabatan.nama',function($model){
                return $model->jenis_pegawai.'/'.$model->jabatan->nama;
            })
            ->editColumn('tanggal_lahir',function($model){
                return $model->usia();
            })
            ->editColumn('foto',function($model){
                if(is_null($model->foto)){
                    return "-";
                }
                return "<img class='img img-thumbnail' src='".route('media.get',[$model->sekolah_id,'foto-pegawai',$model->foto])."'/>";
            })
            ->rawColumns(['action','jabatan.nama','foto']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SekolahTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Pegawai::query()
                        ->with('sekolah','jabatan','lembaga');
        if(!is_null(Auth::User()->sekolah_id)){
            $query = Pegawai::where('pegawai.sekolah_id',Auth::User()->sekolah_id)
                            ->select('pegawai.*','pegawai.nama')
                            ->with('sekolah','jabatan','lembaga');

        }
        if(request()->has('nama')){
            $query->where('pegawai.nama','LIKE','%'.request()->nama.'%');
        }
        if(request()->has('lembaga_id')){
            $query->where('pegawai.lembaga_id',request()->lembaga_id);
        }
        if(request()->has('jenis_pegawai')){
            $query->where('pegawai.jenis_pegawai',request()->jenis_pegawai);
        }
        if(request()->has('jabatan_id')){
            $query->where('pegawai.jabatan_id',request()->jabatan_id);
        }
        if(request()->has('status_kepegawaian')){
            $query->where('status_kepegawaian',request()->status_kepegawaian);
        }
        if(request()->has('status_perkawinan')){
            $query->where('status_perkawinan',request()->status_perkawinan);
        }
        if(request()->has('pendidikan_terakhir')){
            $query->where('pendidikan_terakhir',request()->pendidikan_terakhir);
        }
        if(request()->has('status_kerja')){
            if(request()->status_kerja == "cuti"){
                $query->cuti();
            }
            if(request()->status_kerja == "keluar"){
                $query->nonAktif();
            }
            if(request()->status_kerja == "kerja"){
                $query->kerja();
            }
            if(request()->status_kerja == "seluruh"){
                $query->seluruhStatusKerja();
            }
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->ajax('')
                ->parameters([
                    'dom' => 'lfrtip',
                    'lengthMenu' => [ 10, 50, 100, 200, 300, 500 ],
                    'fixedColumns'=> true,
                    'scrollX'=> true,
                    'order' => [
                        [2,'asc'],[5,'asc']
                    ]
                ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if(is_null(Auth::User()->sekolah_id)){
            return [
                'action' => [
                    'className' => 'text-center'
                ],
                'action' => [
                    'width' => "2%",
                    'className' => 'text-center'
                ],
                'foto' => [
                    'searchable' => false,
                    'orderable' => false,
                    'width' => "5%"
                ],
                'nama' => [
                    'width' => "20%"
                ],
                'nip' => [
                    'width' => "8%"
                ],
                'sekolah.nama' => [
                    'title' => 'Sekolah'
                ],
                'jabatan.nama' => [
                    'title' => 'Jabatan',
                    'width' => '15%'
                ],
                'lembaga.nama' => [
                    'title' => 'Lembaga',
                    'width' => '10%'
                ],
                'tanggal_lahir' => [
                    'title' => 'usia',
                    'width' => '5%'
                ]
            ];
        }
        return [
            'action' => [
                'width' => "2%",
                'className' => 'text-center'
            ],
            'foto' => [
                'searchable' => false,
                'orderable' => false,
                'width' => "5%"
            ],
            'nama' => [
                'width' => "20%"
            ],
            'nip' => [
                'width' => "8%"
            ],
            'jabatan.nama' => [
                'title' => 'Jabatan',
                'width' => '15%'
            ],
            'lembaga.nama' => [
                'title' => 'Lembaga',
                'width' => '10%'
            ],
            'tanggal_lahir' => [
                'title' => 'usia',
                'width' => '5%'
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Pegawai Table_' . date('YmdHis');
    }
}

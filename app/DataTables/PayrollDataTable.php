<?php

namespace App\DataTables;

use App\Models\Payroll;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Auth;

class PayrollDataTable extends DataTable
{
    protected $request = [];

    public function __construct(Request $request){
        $this->request = $request;
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('bulan',function($model){
                return bulan($model->bulan);
            })
            ->addColumn('#','payroll.action.button')
            ->rawColumns(['#']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SekolahTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Payroll::query();
        if(Auth::User()->sekolah_id){
            $query = Payroll::query()
                            ->where('sekolah_id',Auth::User()->sekolah_id);
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->ajax('')
                ->parameters([
                    'dom' => 'lBfrtip',
                    'lengthMenu' => [ 50, 100, 200, 300, 500 ],
                ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => [
                'width' => "5%",
                'orderable' => false
            ],
            'lembaga',
            'jenis_pegawai',
            'bulan',
            'tahun'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PayrollTable_' . date('YmdHis');
    }
}

<?php

namespace App\DataTables;

use App\Models\Sekolah;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Auth;
class SekolahDataTable extends DataTable
{
    protected $request = [];

    public function __construct(Request $request){
        $this->request = $request;
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables()
            ->eloquent($this->query())
            ->addColumn('action','sekolah.action.button')
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SekolahTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Sekolah::query();
        if(Auth::User()->role->name == "kepala-sekolah" || Auth::User()->role->name == "super-user-sekolah"){
            $query = Sekolah::query()
                            ->where('id',Auth::User()->sekolah_id);
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->ajax('')
                ->parameters([
                    'dom' => 'lBfrtip',
                    'lengthMenu' => [ 50, 100, 200, 300, 500 ],
                ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'action' => [
                'width' => "30%"
            ],
            'nama',
            'alamat'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'SekolahTable_' . date('YmdHis');
    }
}

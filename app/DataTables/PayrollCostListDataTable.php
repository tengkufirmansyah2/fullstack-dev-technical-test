<?php

namespace App\DataTables;

use App\Models\PayrollCostList;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;
use Auth;

class PayrollCostListDataTable extends DataTable
{
    protected $request = [];

    public function __construct(Request $request){
        $this->request = $request;
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('biaya',function($model){
                return "Rp. ".number_format($model->biaya);
            })
            ->addColumn('action','payroll-cost.action.button')
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SekolahTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = PayrollCostList::query()
                                ->select('payroll_cost_list.*')
                                ->with('kategori');
        if(Auth::User()->sekolah_id){
            $query = PayrollCostList::query()
                            ->select('payroll_cost_list.*')
                            ->with('kategori')
                            ->where('payroll_cost_list.sekolah_id',Auth::User()->sekolah_id);
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                ->columns($this->getColumns())
                ->ajax('')
                ->parameters([
                    'dom' => 'lBfrtip',
                    'lengthMenu' => [ 50, 100, 200, 300, 500 ],
                ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'kategori.nama' => [
                'title' => 'kategori'
            ],
            'nama',
            'biaya',
            'action' => [
                'width' => "20%"
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PayrollCostListTable_' . date('YmdHis');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DaftarPegawaiRequest;
use App\Http\Requests\RegisterAkunRequest;
use App\Models\Pegawai;
use App\Models\Role;
use App\Models\RegistrasiKonfirmasi;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Str;
use Mail;
use Carbon\Carbon;
use DB;
use Hash;
class RegisterCtrl extends Controller
{
    public function daftar(){
        return view('auth.opsi-register');
    }
    public function daftarPegawai(){
        return view('auth.register-pegawai');
    }
    public function checkDaftarPegawai(DaftarPegawaiRequest $request){
        $pegawai = Pegawai::where('nip',$request->nip)
                          ->where('tanggal_lahir',$request->tanggal_lahir)
                          ->first();
        if(empty($pegawai)){
            alert('danger','Data tidak ditemukan!');
            return back();
        }
        if(!is_null($pegawai->user_id)){
            alert('danger','Akun pegawai sudah terdaftar!');
            return back();
        }
        DB::beginTransaction();
        try {
            //flag kalo ada link sebelumnya yang belum kepake, supaya gaada yg pake link lama
            $check = RegistrasiKonfirmasi::where('keterangan','pegawai')
                                            ->where('related_id',$pegawai->id)
                                            ->where('is_used',0)
                                            ->update([
                                                'is_used' => 1
                                            ]);
            $konfirmasi = RegistrasiKonfirmasi::create([
                'token' => Crypt::encryptString(Str::uuid()),
                'email' => $request->email,
                'keterangan' => 'pegawai',
                'related_id' => $pegawai->id,
                'expire_at' => Carbon::now()->addDays(1)
            ]);
            $link = route('daftar.buat-akun',[$konfirmasi->id,$konfirmasi->token]);
            dispatch(new \App\Jobs\RegistrasiEmail($request->email,$link));
            DB::commit();
            alert('success','Informasi anda telah berhasil dikonfirmasi! Silahkan cek email anda.');
            return back();
        } catch (\Exception $e) {
            DB::rollback();
            alert('danger','Terjadi kesalahan! Error:'.$e->getMessage());
            return back();
        }
        
    }
    public function verifyRegister($id,$token){
        $check = RegistrasiKonfirmasi::where('id',$id)
                                     ->where('token',$token)
                                     ->where('expire_at','>',Carbon::now()->toDateString())
                                     ->firstOrFail();
        $nama = null;
        if($check->keterangan == "pegawai"){
            $pegawai = Pegawai::where('id',$check->related_id)->first();
            if(empty($pegawai)){
                alert('danger','Terjadi ketidaksesuaian pada data pegawai!');
                return redirect()->to(route('login'));
            }
            $nama = $pegawai->nama;
        }
        return view('auth.register-akun',compact('nama'));
    }
    public function verifyRegisterNow(RegisterAkunRequest $request, $id, $token){
        $check = RegistrasiKonfirmasi::where('id',$id)
                                     ->where('token',$token)
                                     ->where('expire_at','>',Carbon::now()->toDateString())
                                     ->first();
        if(empty($check)){
            alert('danger','Token telah digunakan, silahkan melakukan daftar ulang!');
            return redirect()->to(route('login'));
        }
        DB::beginTransaction();
        try {
            if($check->keterangan == "pegawai"){
                $pegawai = Pegawai::where('id',$check->related_id)->first();
                if(empty($pegawai)){
                    alert('danger','Terjadi ketidaksesuaian pada data pegawai!');
                    return back();
                }
                if(!is_null($pegawai->user_id)){
                    alert('danger','Pegawai Sudah Memiliki Akun!');
                    return back();
                }
                if($pegawai->jenis_pegawai == "Guru"){
                    $role = Role::where('name','guru')->first()->id;
                }else{
                    $role = Role::where('name','pegawai')->first()->id;
                }
                $createUser = User::create([
                    'name' => $pegawai->nama,
                    'email' => $check->email,
                    'username' => $request->username,
                    'sekolah_id' => $pegawai->sekolah_id,
                    'password' => Hash::make($request->password),
                    'role_id' => $role,
                    'email_verified_at' => Carbon::now()
                ]);
                $updatePegawai = Pegawai::where('id',$check->related_id)
                                        ->update([
                                            'user_id' => $createUser->id
                                        ]);
            }
            $flagToken =  RegistrasiKonfirmasi::where('id',$id)
                                                    ->where('token',$token)
                                                    ->update([
                                                        'is_used' => 1
                                                    ]);
                                            
            DB::commit();
            alert('success','Akun Sudah Berhasil didaftarkan! Silahkan melakukan login.');
            return redirect()->to(route('login'));
        } catch (\Exception $e) {
            DB::rollback();
            alert('danger','Terjadi kesalahan! Error:'.$e->getMessage());
            return back();
        }
        
    }
}

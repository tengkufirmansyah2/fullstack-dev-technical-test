<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UrlRequest;
use App\Http\Resources\DefaultCollection;
use App\Providers\PermissionsProvider;

use App\Http\Requests\BlogsRequest;

use App\Models\Blogs;
use App\Models\CommentBlogs;

use DB, Session, Auth, Image;

class BlogsCtrl extends Controller
{
    public function __construct()
    {
        $this->title = "Blogs";

        $this->middleware('permission:blogs-view',['only' => 'index']);
        $this->middleware('permission:blogs-edit',['only' => ['edit', 'destroy']]);
        $this->middleware('permission:blogs-update',['only' => 'update']);
        $this->middleware('permission:blogs-create',['only' => ['create','store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;

        if($request->ajax()){
            $url = Blogs::get()->toArray();
            return response()->json(new DefaultCollection($url));
        }
        return view('blogs.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = $this->title.' Baru';
        $edit = false;
        return view('blogs.create',compact('edit','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogsRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {

                $data = New Blogs;
                $data->title = $request['title'];
                $data->description = $request['description'];

                if(isset($request['image'])){
                    $image = $this->images($request['image']);
                    $data->image_name = $image;
                    $data->image_path = preg_replace('#^https?://#i', '', url('storage/blogs'));
                };

                $data->created_by = Auth::user()->id;
                $data->save();

                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'View '.$this->title;

        $data = Blogs::where('id',$id)->with('comments')->firstOrFail();
        // return $city;
        return view('blogs.view',compact('title','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = 'Edit '.$this->title;

        $edit = true;
        $data = Blogs::where('id',$id)->firstOrFail();
        // return $hotel;
        return view('blogs.create',compact('edit','title','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogsRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Blogs::find($id);
            $data->title = $request['title'];
            $data->description = $request['description'];

            if(isset($request['image'])){
                $image = $this->images($request['image']);
                $data->image_name = $image;
                $data->image_path = preg_replace('#^https?://#i', '', url('storage/blogs'));
            };

            $data->updated_by = Auth::user()->id;
            $data->save();

            DB::commit();
            return response()->json([
                'status' => 'success',
                'message' =>  ''
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                $data = Blogs::find($id);
                $data->delete();

                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }

    public function images($val)
    {
        $filenamewithextension = $val->getClientOriginalName();
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        $extension = $val->getClientOriginalExtension();
  
        $filenametostore = $filename.'_'.time().'.'.$extension;

        $val->storeAs('public/blogs', $filenametostore);
        $val->storeAs('public/blogs/thumbnail', $filenametostore);

        $mediumthumbnailpath = public_path('storage/blogs/thumbnail/'.$filenametostore);
        $this->createThumbnail($mediumthumbnailpath, $filenametostore, 300, 300);
        return $filenametostore;
    }

    public function createThumbnail($path, $file, $width, $height)
    {
        $img = Image::make($path);
        if ($img->width() > $width) { 
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        if ($img->height() > $height) {
            $img->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();
            }); 
        }

        $img->resizeCanvas($width, $height, 'center', false, '#ffffff');
        $img->save($path);
    }

    public function getBlogs(){
        $data = Blogs::get();
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function getDetailBlogs($id){
        $data = Blogs::where('id', $id)->with('comments')->first();
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function commentBlogs(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = New CommentBlogs;
            $data->note = $request['comment'];
            $data->blog_id = $id;
            $data->save();

            DB::commit();
            return response()->json([
                'status' => 'success',
                'data' =>  ''
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'data' =>  $e->getMessage()
            ],500);
        }
    }
}

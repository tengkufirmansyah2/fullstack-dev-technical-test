<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ErrorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($message)
    {
        return [
            'status' => 'error',
            'message' =>  $message
        ];
    }
    public function toResponse($request){
        return parent::toResponse($request)->setStatusCode(500);
    }
}

<?php
namespace App\CustomClass;

class KtUtilityDataTable {

	public $test = "ayam";
	private $input = array();

	private $output = array();

	private $orderby = array();

	public function __construct( $input )
	{
		$this->output = $this->input = $input;
	}

	public function get_input()
	{
		return $this->input;
	}

	public function get_output()
	{
		return $this->output;
	}

	public function filter( $args = array(), $operator = 'AND' ){
		if ( empty( $args ) ) {
			return $this->output;
		}

		$operator = strtoupper( $operator );

		if ( ! in_array( $operator, array( 'AND', 'OR', 'NOT' ), true ) ) {
			return array();
		}

		$count    = count( $args );
		$filtered = array();

		foreach ( $this->output as $key => $obj ) {
			$to_match = (array)$obj;

			$matched = 0;
			foreach ( $args as $m_key => $m_value ) {
				if ( array_key_exists( $m_key, $to_match ) && $m_value == $to_match[ $m_key ] ) {
					$matched++;
				}
			}

			if (
				( 'AND' == $operator && $matched == $count ) ||
				( 'OR' == $operator && $matched > 0 ) ||
				( 'NOT' == $operator && 0 == $matched )
			) {
				$filtered[ $key ] = $obj;
			}
		}

		$this->output = $filtered;

		return $this->output;
	}
	public function pluck( $field, $index_key = null ){
		if ( ! $index_key ) {
			/*
			 * This is simple. Could at some point wrap array_column()
			 * if we knew we had an array of arrays.
			 */
			foreach ( $this->output as $key => $value ) {
				if ( is_object( $value ) ) {
					$this->output[ $key ] = $value->$field;
				} else {
					$this->output[ $key ] = $value[ $field ];
				}
			}

			return $this->output;
		}

		/*
		 * When index_key is not set for a particular item, push the value
		 * to the end of the stack. This is how array_column() behaves.
		 */
		$newlist = array();
		foreach ( $this->output as $value ) {
			if ( is_object( $value ) ) {
				if ( isset( $value->$index_key ) ) {
					$newlist[ $value->$index_key ] = $value->$field;
				} else {
					$newlist[] = $value->$field;
				}
			} else {
				if ( isset( $value[ $index_key ] ) ) {
					$newlist[ $value[ $index_key ] ] = $value[ $field ];
				} else {
					$newlist[] = $value[ $field ];
				}
			}
		}

		$this->output = $newlist;

		return $this->output;
	}
	public function sort( $orderby = array(), $order = 'ASC', $preserve_keys = false ){
		if ( empty( $orderby ) ) {
			return $this->output;
		}

		if ( is_string( $orderby ) ) {
			$orderby = array( $orderby => $order );
		}

		foreach ( $orderby as $field => $direction ) {
			$orderby[ $field ] = 'DESC' === strtoupper( $direction ) ? 'DESC' : 'ASC';
		}

		$this->orderby = $orderby;

		if ( $preserve_keys ) {
			uasort( $this->output, array( $this, 'sort_callback' ) );
		} else {
			usort( $this->output, array( $this, 'sort_callback' ) );
		}

		$this->orderby = array();

		return $this->output;
	}
	private function sort_callback( $a, $b ){
		if ( empty( $this->orderby ) ) {
			return 0;
		}

		$a = (array)$a;
		$b = (array)$b;

		foreach ( $this->orderby as $field => $direction ) {
			if ( ! isset( $a[ $field ] ) || ! isset( $b[ $field ] ) ) {
				continue;
			}

			if ( $a[ $field ] == $b[ $field ] ) {
				continue;
			}

			$results = 'DESC' === $direction ? array( 1, -1 ) : array( -1, 1 );

			if ( is_numeric( $a[ $field ] ) && is_numeric( $b[ $field ] ) ) {
				return ( $a[ $field ] < $b[ $field ] ) ? $results[ 0 ] : $results[ 1 ];
			}

			return 0 > strcmp( $a[ $field ], $b[ $field ] ) ? $results[ 0 ] : $results[ 1 ];
		}

		return 0;
	}
}

function list_filter( $list, $args = array(), $operator = 'AND' )
{
	if ( ! is_array( $list ) ) {
		return array();
	}

	$util = new List_Util( $list );

	return $util->filter( $args, $operator );
}
function test(){
	return "kentut";
}
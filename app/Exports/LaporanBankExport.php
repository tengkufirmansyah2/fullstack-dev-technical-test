<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Auth;

class LaporanBankExport implements FromView
{
    public function __construct($input)
    {
        $this->input = $input;
    }

    public function view(): View
    {
        return view('keuangan.laporan_bank.proses', $this->input);
    }
}
<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Auth;

class LaporanBulananExport implements FromView
{
    public function __construct($input)
    {
        $this->input = $input;
    }

    public function view(): View
    {
        return view('keuangan.laporan_bulanan.proses', $this->input);
    }
}
<?php
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\AppSettings;
use App\Models\Url;
use App\Models\Pegawai;

if (!function_exists('app_setting')) {
    function app_setting(){
            $appSettings = \DB::table('app_settings')
                                    ->where('status','=','0')
                                    ->get()
                                    ->toArray();
            $appSettings = AppSettings::hydrate($appSettings);
            $settings = [];
            foreach($appSettings as $key => $r){
                $settings += [str_replace(".","_",$r->keyField) => $r];
            }
            return $settings;
    }
}


if (!function_exists('kt_datatable')) {
    function kt_datatable($title, $url)
    {
        return view('components.kt_datatable', compact('title', 'url'));
    }
}

if (!function_exists('master_url')) {
    function master_url($title)
    {
        $user = Auth::user();
        $url = Url::select('url.id','url.parent_id','url.url','url.icon','url.position','url.name')
                ->join('url_access','url_access.url_id','url.id')
                ->join('users','users.role_id','url_access.role_id')
                ->where('position','=', $title)
                ->where('users.id', $user->id)
                ->orderBy('order','asc')
                ->whereNull('parent_id')
                ->with('children')
                ->get();
        return $url;
    }
}

if (!function_exists('breadcrumb')) {
    function breadcrumb()
    {
        $breadcrumb = "";
        $path = explode("/", request()->path());
        $last = count($path)-1;
        $link = "";
        foreach ($path as $key => $val) {
            $link .= '/'.$val;
            if ($key == 0) {
                $breadcrumb .= '<li class="breadcrumb-item text-muted"><a href="'.url($link).'" class="text-muted">'.$val.'</a></li>';
            }else{
                $breadcrumb .= '<li class="breadcrumb-item text-muted"><a href="'.url($link).'" class="text-muted">'.$val.'</a></li>';
            }

        }
        return $breadcrumb;
    }
}

if (!function_exists('beautiDate')) {
    function beautiDate($date)
    {
        $date = date('Y-m-d', strtotime($date));
        $ex = explode('-', $date);
        $year = $ex[0];
        $month = beautiMonth($ex[1]);
        $day = $ex[2];
        return $day.' '.$month.' '.$year;
        return $breadcrumb;
    }
}

function beautiMonth($month){
    $list = ['Januari', 'Februari', 'Maret', 'April', 'May', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    $data = $list[$month-1];
    return $data;
}

function alert($type,$message){
    Session()->flash('alert',$type);
    Session()->flash('message',$message);
}
function ktDatatable($data,$req){
    $alldata = $data;
    $datatable = array_merge(array('pagination' => array(), 'sort' => array(), 'query' => array()), $req);
    // search filter by keywords
    $filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch']) ? $datatable['query']['generalSearch'] : '';
    if (!empty($filter)) {
        $data = array_filter($data, function ($a) use ($filter) {
            return (boolean)preg_grep("/$filter/i", (array)$a);
        });
        unset($datatable['query']['generalSearch']);
    }
    
    // filter by field query
    $query = isset($datatable['query']) && is_array($datatable['query']) ? $datatable['query'] : null;
    if (is_array($query)) {
        $query = array_filter($query);
        foreach ($query as $key => $val) {
            $data = list_filter($data, array($key => $val));
        }
    }
    $sort = !empty($datatable['sort']['sort']) ? $datatable['sort']['sort'] : 'asc';
    $field = !empty($datatable['sort']['field']) ? $datatable['sort']['field'] : 'id';
    
    $meta = array();
    $page = !empty($datatable['pagination']['page']) ? (int)$datatable['pagination']['page'] : 1;
    $perpage = !empty($datatable['pagination']['perpage']) ? (int)$datatable['pagination']['perpage'] : -1;
    
    $pages = 1;
    $total = count($data); // total items in array
    usort($data, function ($a, $b) use ($sort, $field) {
        if (!isset($a->$field) || !isset($b->$field)) {
            return -1;
        }
    
        if ($sort === 'asc') {
            return $a->$field > $b->$field ? 1 : -1;
        }
    
        return $a->$field < $b->$field ? 1 : -1;
    });
    
    // $perpage 0; get all data
    if ($perpage > 0) {
        $pages = ceil($total / $perpage); // calculate total pages
        $page = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
        $page = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
        $offset = ($page - 1) * $perpage;
        if ($offset < 0) {
            $offset = 0;
        }
    
        $data = array_slice($data, $offset, $perpage, true);
    }
    $meta = [
        "page" => $page,
        "pages" => $pages,
        "perpage" => $perpage,
        "total" => $total
    ];
    return $meta;
}
function pendidikanTerakhir(){
    return [
        'SD',
        'SMP',
        'SMA',
        'SMK',
        'D1',
        'D2',
        'D3',
        'S1',
        'S2',
        'S3'
    ];
}
function slugify($text){
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}
function nip($sekolah_id,$jenis_pegawai,$tmt){
    $date = date("dmy", strtotime($tmt));
    $month = date("m", strtotime($tmt));
    $year = date("Y", strtotime($tmt));
    if($jenis_pegawai == "Guru"){
        $first = 1;
    }else{
        $first = 2;
    }
    $pegawai = App\Models\Pegawai::where('sekolah_id',$sekolah_id)
                                   ->whereMonth('tmt',$month)
                                   ->whereYear('tmt',$year)
                                   ->where('jenis_pegawai',$jenis_pegawai)
                                   ->orderBy('tmt','desc')
                                   ->orderBy('nip','desc')
                                   ->first();
    if(empty($pegawai)){
        return $first.$date.'001';
    }
    $rawNomorBelakang = substr($pegawai->nip,7,8) + 1;
    $nomorbelakang = str_pad($rawNomorBelakang,3,"0", STR_PAD_LEFT);
    return $first.$date.$nomorbelakang;

}
if (!function_exists('H_generateCode')) {
    function H_generateCode($type, $kat, $model)
    {
        $check = $model::whereYear('created_at', date('Y'))
                        ->where('jenis_jurnal','=', $kat)
                        ->where('sekolah_id','=', getLembagaByUser()->sekolah_id)
                        ->where('lembaga_id','=', getLembagaByUser()->lembaga_id)
                        ->orderBy('created_at', 'desc');
        if ($check->first() !== null) {
            $code = $check->first()->kode;
            $ex = explode('-', $code);
            $num = (int) $ex[count($ex) - 1];
            $nex = sprintf("%05s", $num+1);
            if ($kat == 'Debit') {
                $code = $type.'-D-'.date('Y').'-'.$nex;
            } else {
                $code = $type.'-K-'.date('Y').'-'.$nex;
            }
        }else{
            if ($kat == 'Debit') {
                $code = $type.'-D-'.date('Y').'-00001';
            } else {
                $code = $type.'-K-'.date('Y').'-00001';
            }
        }
        return $code;
    }
}

if (!function_exists('H_generateMasterAkun')) {
    function H_generateMasterAkun($parent, $model)
    {
        $check = $model::orderBy('kode', 'desc');
        if ($parent != null) {
            $check->where('parent_id', '=', $parent);
            $in = $model::where('id', $parent)->first();
            if ($check->first() !== null) {
                $code = $check->first()->kode;
                $ex = explode('.', $code);
                $num = (int) $ex[count($ex) - 1];
                if (count($ex) > 2) {
                    $nex = sprintf("%03s", $num+1);
                } else {
                    $nex = sprintf("%02s", $num+1);
                }
                $code = $in->kode.'.'.$nex;
            }else{
                $code = $in->kode;
                $ex = explode('.', $code);
                $num = (int) $ex[count($ex) - 1];
                if (count($ex) > 2) {
                    $code = $in->kode.'.001';
                } else {
                    $code = $in->kode.'.01';
                }
            }
        } else {
            if ($check->first() !== null) {
                $code = $check->first()->kode;
                $code = $code+1;
            }else{
                $code = 1;
            }
        }
        return $code;
    }
}
function permissionCheck($request,$modul){
    if (\App\Providers\PermissionsProvider::has($request,$modul)){
        return true;
    }   
    return false;
}
function checkBerkasPegawai($id,$related_id){
    $berkas = App\Models\Berkas::where('berkas_settings_id',$id)
                                ->where('related_id',$related_id)
                                ->first();
    if(!empty($berkas)){
        return true;
    }
    return false;
}
function getBerkasPegawai($id,$related_id){
    $berkas = App\Models\Berkas::where('berkas_settings_id',$id)
                                ->where('related_id',$related_id)
                                ->first();
    if(!empty($berkas)){
        return $berkas->berkas;
    }
}
function transformMoney($val){
    $a = preg_replace("[,]", "", preg_replace("[Rp. ]", "", $val));
    $a = preg_replace("[Rp ]","",$a);
    return str_replace(".", "", $a);
}
function bulan($number){
    if($number == 1){
        return 'Januari';
    }
    if($number == 2){
        return 'Februari';
    }
    if($number == 3){
        return 'Maret';
    }
    if($number == 4){
        return 'April';
    }
    if($number == 5){
        return 'Mei';
    }
    if($number == 6){
        return 'Juni';
    }
    if($number == 7){
        return 'Juli';
    }
    if($number == 8){
        return 'Agustus';
    }
    if($number == 9){
        return 'September';
    }
    if($number == 10){
        return 'Oktober';
    }
    if($number == 11){
        return 'November';
    }
    if($number == 12){
        return 'Desember';
    }
}
function totalTmt($pegawai_id){
    $tmt = DB::table('pegawai')->where('id',$pegawai_id)->first()->tmt;
    return \Carbon\Carbon::now()->diffInYears($tmt);
}

function getLembagaByUser(){
    $data = Pegawai::where('user_id', Auth::user()->id)->first();
    return $data;
}
function daftarBank(){
    $bank = [
        'Bank BSI',
        'Bank Mandiri',
        'Bank BNI',
        'Bank BTPN',
        'Bank Danamon',
        'Bank Permata',
        'Bank BCA',
        'Bank Maybank',
        'Bank CIMB Niaga',
        'Bank OCBC',
    ];
    return $bank;
}
<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class UrlAccess extends Model
{
    use Uuid;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'url_access';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['url_id','role_id'];

    public $incrementing = false;
}

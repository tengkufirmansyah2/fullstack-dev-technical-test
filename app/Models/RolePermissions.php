<?php

namespace App\Models;

use App\Http\Middleware\Permission;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class RolePermissions extends Model
{
    use Uuid;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_permissions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['permission_id','role_id','created_by','updated_by'];

    public $incrementing = false;
    public function detail(){
        return $this->belongsTo(Permissions::class,'permission_id');
    }
    
}

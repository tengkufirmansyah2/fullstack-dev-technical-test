<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Auth;

class Role extends Model
{
    use Uuid;

    protected $table = 'role';

    protected $primaryKey = 'id';
    
    protected $fillable = ['name'];

    // disabled timestamps data
    public $timestamps = true;

    // disable update col id
    protected $guarded = ['id'];
    public $incrementing = false;

}
<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Blogs extends Model
{
    use Uuid;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'image_name',
        'image_path',
        'created_by',
        'updated_by',

    ];

    public function comments()
    {   
        return $this->hasMany(CommentBlogs::class, 'blog_id')->orderBy('created_at', 'desc');
    }
}

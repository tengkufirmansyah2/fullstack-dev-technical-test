<?php

namespace App\Imports;

use App\Models\Keuangan\DataBank;
use App\Models\Keuangan\DataBankDetail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToCollection;
use DB, Session;
class DataBankImport implements ToCollection
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function collection(Collection $rows)
    {
        $period = explode('-',$rows[4][1]);
        $start = date('Y-m-d', strtotime($period[0]));
        $end = date('Y-m-d', strtotime($period[1]));

        DB::beginTransaction();
        try {
            $data = DataBank::create([
                    'keterangan' => $rows[1][0],
                    'nama' => $rows[2][1],
                    'alamat' => $rows[3][1],
                    'period_start' => $start,
                    'period_end' => $end,
                    'saldo_awal' => str_replace("-","",$rows[5][1]),
                    'saldo_akhir' => str_replace("-","",$rows[6][1]),
                    'total_debet' => str_replace("-","",$rows[7][1]),
                    'total_kredit' => str_replace("-","",$rows[8][1]),
                    'sekolah_id' => getLembagaByUser()->sekolah_id,
                    'lembaga_id' => getLembagaByUser()->lembaga_id,
                ]);
            $newItems = [];
            for ($i=11; $rows[$i][1] != null ; $i++) { 
                if ($rows[$i][1] != null) {
                    $input = [
                        'keuangan_data_bank_id' => $data->id,
                        'trx_id' => str_replace(" ","", $rows[$i][0]),
                        'tanggal' => date('Y-m-d', strtotime($rows[$i][1])),
                        'trx_time' => date('H:i:s', strtotime($rows[$i][2])),
                        'dk' => str_replace(" ","", $rows[$i][3]),
                        'mutasi' => (float)$rows[$i][4],
                        'saldo' => (float)$rows[$i][5],
                        'keterangan' => $rows[$i][6],
                    ];
                    $check = DataBankDetail::where('tanggal','=', date('Y-m-d', strtotime($rows[$i][1])))->where('trx_time','=', date('H:i:s', strtotime($rows[$i][2])))->first();
                    if (!$check) {
                        $newItems[] = $input;
                    }
                }
            }
            $data->dataBankDetail()->createMany($newItems);
            DB::commit();
            Session::flash('alert');
            Session::flash('message', 'Data berhasil ter import !');
            Session::flash('alert-class', 'success'); 
            Session::flash('alert-icon', 'interface-10'); 
            return $data;
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('alert');
            Session::flash('message', 'Data gagal ter import !');
            Session::flash('alert-class', 'danger'); 
            Session::flash('alert-icon', 'warning'); 
        }
    }
}